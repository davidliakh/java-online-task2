package com.rumors;
import java.util.*;
/**
 * This class describe problem which are rised in homewrk.
 * Calculate probability that every one hear the rumor.
 * Main class when the program starts and ends)))
 * @author David Liakh.
 * */

public class gossip_Party {
    /***
     *It combines methods which resolve problem.
     * @param args form the command line .
     */

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter how much people going to the the party(it must be more than 1 guest): ");

        int n = 0;
        try {
            n = sc.nextInt();
            if (n <= 1) {
                System.err.println("Error: Enter a valid Integer value > 1.");
            }
        } catch (InputMismatchException e) {
            System.err.println("Error: Enter a valid number.");
        }
        gossip_Party s1 = new gossip_Party();
        s1.probability(n);
    }
    /**
     *It contains the algortihm for calculate probability
     * @param n amount of guests from main method.
     */

    public void probability (int n) {
        final int t= 100;
        int Times = 0;
        int peopleReached = 0;

        for (int i = 0; i < t; i++) {
            boolean guests[] = new boolean[n];
            guests[1] = true;
            boolean alreadyHeard = false;
            int nextPerson = -1;
            int currentPerson = 1;
            while (!alreadyHeard) {
                nextPerson = 1 + (int) (Math.random()*(n-1));
                if (nextPerson == currentPerson) {
                    while (nextPerson == currentPerson)
                        nextPerson = 1 + (int) (Math.random() * (n - 1));
                }
                if (guests[nextPerson])
                {
                    if (rumorSpreaded(guests))
                        Times++;
                    peopleReached += countPeopleReached(guests);
                    alreadyHeard = true;
                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }

        System.out.println("Probability that everyone will hear rumor except Alice in " + t + " attempts: " +
                (double) Times / t);
        System.out.println("Average amount of people that rumor reached is: " + peopleReached / t);
    }
    /**
     * it contains counter for count of people which heard the rumor.
     * @param arr array of guests.
     * @return count.
     */

    public static int countPeopleReached(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }
    /**
     * It contains boolean method which checking the people who heard this rumor.
     * @param arr og guests.
     * @return boolean flag.
     */
    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }
}
